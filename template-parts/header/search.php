<div class="site-search">
    <a href="#" class="js-search-trigger">
        <img src="<?php bloginfo('template_directory'); ?>/images/icon-search.svg" width="20" height="20" alt="Search the Site" />
    </a>
</div>