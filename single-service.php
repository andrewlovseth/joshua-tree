<?php get_header(); ?>

    <?php get_template_part('templates/single-service/hero'); ?>

    <?php get_template_part('templates/single-service/service-info'); ?>

    <?php get_template_part('templates/single-service/featured-projects'); ?>

    <?php get_template_part('template-parts/global/news-grid'); ?>

<?php get_footer(); ?>