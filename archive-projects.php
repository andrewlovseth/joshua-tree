<?php get_header(); ?>

    <?php get_template_part('templates/archive-projects/page-header'); ?>

    <?php get_template_part('templates/archive-projects/filters'); ?>

    <?php get_template_part('templates/archive-projects/projects-grid'); ?>

<?php get_footer(); ?>