	</main> <!-- .site-content -->

	<?php get_template_part('template-parts/footer/sign-up'); ?>

	<footer class="site-footer grid">

		<?php get_template_part('template-parts/footer/nav'); ?>

		<?php get_template_part('template-parts/footer/utilities'); ?>

	</footer>

	<?php //get_template_part('template-parts/footer/top-btn'); ?>

<?php wp_footer(); ?>

</div> <!-- .site -->

</body>
</html>