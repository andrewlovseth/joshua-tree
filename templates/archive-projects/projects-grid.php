<section class="projects grid">
    <?php
        echo do_shortcode('[ajax_load_more id="projects" transition_container="false" theme_repeater="project.php" container_type="div" css_classes="projects-grid" filters_url="false" target="projects_filter" filters="true" preloaded="true" preloaded_amount="24" post_type="projects" posts_per_page="24" scroll="false"]');
    ?>
    <div class="alm-results-text"></div>
</section>