<?php 

/*

    Template Name: Market (2024)
    Template Post Type: market

*/

get_header(); ?>

    <?php get_template_part('templates/market/hero'); ?>

    <?php get_template_part('templates/market/sub-nav'); ?>

    <?php get_template_part('templates/market/about'); ?>

    <?php get_template_part('templates/market/leaders'); ?>

    <?php get_template_part('templates/market/projects'); ?>

    <?php get_template_part('template-parts/global/news-grid'); ?>

<?php get_footer(); ?>

