<?php 

/*

    Template Name: Newsletter

*/

get_header(); ?>

    <?php get_template_part('templates/about/page-header'); ?>

    <?php get_template_part('templates/newsletter/form'); ?>

<?php get_footer(); ?>