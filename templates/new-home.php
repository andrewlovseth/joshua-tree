<?php 

/*

    Template Name: New Home

*/

get_header(); ?>


    <?php get_template_part('templates/new-home/about'); ?>

    <?php get_template_part('templates/new-home/featured-projects'); ?>

    <?php get_template_part('templates/new-home/esa'); ?>

    <?php get_template_part('template-parts/global/news-grid'); ?>

<?php get_footer(); ?>