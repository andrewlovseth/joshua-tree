<?php 

/*

    Template Name: Sub Market
    Template Post Type: market

*/

get_header(); ?>

    <?php get_template_part('templates/sub-market/about'); ?>

    <?php get_template_part('templates/market/leaders'); ?>

    <?php get_template_part('templates/market/projects'); ?>

    <?php get_template_part('template-parts/global/news-grid'); ?>

<?php get_footer(); ?>

