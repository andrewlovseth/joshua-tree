<?php

    $details = get_field('details');
    $about = $details['about'];

?>

<section class="overview grid">

    <div class="copy-2">
        <?php echo $about; ?>
    </div>

</section>