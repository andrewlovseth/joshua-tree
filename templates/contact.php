<?php 

/*

    Template Name: Contact

*/

get_header(); ?>

    <?php get_template_part('templates/about/page-header'); ?>

    <?php get_template_part('templates/contact/offices'); ?>

<?php get_footer(); ?>