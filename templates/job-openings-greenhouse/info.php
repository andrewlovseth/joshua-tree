<section class="info grid">
    <?php get_template_part('templates/job-openings-greenhouse/mission'); ?>

    <?php get_template_part('templates/job-openings-greenhouse/spotlight'); ?>

</section>

<?php get_template_part('templates/job-openings-greenhouse/rich-links'); ?>
