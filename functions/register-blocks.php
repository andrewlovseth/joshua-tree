<?php

/*
    Register Blocks
*/




add_action('acf/init', 'my_register_blocks');

function my_register_blocks() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(array(
            'name'              => 'sidebar',
            'title'             => __('Sidebar'),
            'description'       => __('Inset sidebar inline with copy.'),
            'render_template'   => 'blocks/sidebar/sidebar.php',
            'category'          => 'layout',
            'icon'              => 'welcome-widgets-menus',
            'align'             => 'right',
            'mode'			    => 'preview',
            'supports'          => array(
                'align' => array( 'left', 'right', 'full' ),
                'jsx' => true
            ),
        ));


        acf_register_block_type(array(
            'name'              => 'esa-contacts',
            'title'             => __('ESA Contacts'),
            'description'       => __('Customizable grid of ESA contacts.'),
            'render_template'   => 'blocks/esa-contacts/esa-contacts.php',
            'category'          => 'layout',
            'icon'              => 'dashicons-grid-view',
            'align'             => 'full',
            'mode'			    => 'preview',
            'supports'          => array(
                'jsx' => true
            ),
        ));

        acf_register_block_type(array(
            'name'              => 'esa-hero',
            'title'             => __('ESA Hero'),
            'description'       => __('Customizable ESA hero.'),
            'render_template'   => 'blocks/esa-hero/esa-hero.php',
            'category'          => 'layout',
            'icon'              => 'dashicons-grid-view',
            'align'             => 'full',
            'mode'			    => 'preview',
            'supports'          => array(
                'align' => array('full' ),

                'jsx' => true
            ),
        ));

        acf_register_block_type(array(
            'name'              => 'esa-featured-projects',
            'title'             => __('ESA Featured Projects'),
            'description'       => __('ESA Featured Projects block.'),
            'render_template'   => 'blocks/esa-featured-projects/esa-featured-projects.php',
            'category'          => 'layout',
            'icon'              => 'dashicons-grid-view',
            'align'             => 'full',
            'mode'			    => 'preview',
            'supports'          => array(
                'align' => array('full' ),
                'jsx' => true
            ),
        ));
    }
}