<?php get_header(); ?>

    <section class="page-header grid">
        <h1 class="page-title">This page doesn't seem to exist.</h1>

        <div class="copy copy-2 extended">
            <p>It looks like the link pointing here was faulty. Maybe try searching?</p>
        </div>
    </section>

    <?php get_template_part('template-parts/header/search-navigation'); ?>



<?php get_footer(); ?>